﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace Web.Models
{
    public class DinnerDB : IdentityDbContext<User>
    {
        public DbSet<Dinner> Dinners { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<RSVP> RSVP { get; set; }
        public DbSet<Settings> Settings { get; set; }
        public DbSet<Payment> Payments { get; set; }

        public DinnerDB()
            : base("DinnerDB")
        {
        }
    }

    internal sealed class Configuration : DbMigrationsConfiguration<DinnerDB>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }
    }
}