﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Web.Models
{
    public class Ingredient
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double Quantity { get; set; }
        public ScaleTypes ScaleType { get; set; }
        public decimal CostPerUnit { get; set; }
    }
}
