﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Web.Models
{
    public enum ScaleTypes
    {
        Ounces = 1,
        Grams = 2,
        Milliliters = 3,
        Cups = 4,
        Quarts = 5,
        Teaspoons = 6,
        Tablespoons = 7,
        Dashes = 8
    }
}
