﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Dinner
    {
        public int ID { get; set; }
        public DateTime Date { get; set; }

        private ICollection<RSVP> _guests;
        public virtual ICollection<RSVP> Guests
        {
            get { return _guests ?? (_guests = new Collection<RSVP>()); }
            set { _guests = value; }
        }

        public virtual Recipe Recipe { get; set; }

        public decimal TotalCost
        {
            get
            {
                if (Recipe == null)
                    return 0;
                return Recipe.CostPerPerson * Guests.Count();
            }
        }
    }
}