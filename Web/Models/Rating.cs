﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Web.Models
{
    public class Rating
    {
        public int ID { get; set; }
        public int Stars { get; set; }
        public string Summary { get; set; }
        public virtual User Voter { get; set; }
        public virtual Recipe Recipe { get; set; }
    }
}
