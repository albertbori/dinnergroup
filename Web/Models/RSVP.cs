﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class RSVP
    {
        public int ID { get; set; }
        public virtual User Guest { get; set; }

        public virtual Dinner Dinner { get; set; }
        public RSVPStatuses Status { get; set; }
    }
}