﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Payment
    {
        public int ID { get; set; }
        public virtual User Guest { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
    }
}