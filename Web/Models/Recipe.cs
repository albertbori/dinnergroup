﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Recipe
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual User Author { get; set; }

        private ICollection<Ingredient> _ingredients;
        public virtual ICollection<Ingredient> Ingredients
        {
            get { return _ingredients ?? (_ingredients = new Collection<Ingredient>()); }
            set { _ingredients = value; }
        }

        /// <summary>
        /// The number of people this recipe feeds by default.
        /// </summary>
        public int Feeds { get; set; }

        private ICollection<Rating> _ratings;
        public virtual ICollection<Rating> Ratings
        {
            get
            {
                return _ratings ?? (_ratings = new Collection<Rating>());
            }
            set { _ratings = value; }
        }

        public decimal TotalCost
        {
            get
            {
                return this.Ingredients.Sum(i => i.CostPerUnit * (decimal)i.Quantity);
            }
        }
        public decimal CostPerPerson
        {
            get
            {
                if(Feeds == 0)
                    return 0;

                return TotalCost / Feeds;
            }
        }
    }
}