﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Settings
    {
        public int ID { get; set; }
        public SettingTypes Type { get; set; }
        public string Value { get; set; }
    }
}