﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Web.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        private ICollection<RSVP> _reservations;
        public virtual ICollection<RSVP> Reservations
        {
            get
            {
                return _reservations ?? (_reservations = new Collection<RSVP>());
            }
            set { _reservations = value; }
        }

        private ICollection<Rating> _ratings;
        public virtual ICollection<Rating> Ratings
        {
            get
            {
                return _ratings ?? (_ratings = new Collection<Rating>());
            }
            set { _ratings = value; }
        }

        private ICollection<Recipe> _recipes;
        public virtual ICollection<Recipe> Recipes
        {
            get
            {
                return _recipes ?? (_recipes = new Collection<Recipe>());
            }
            set { _recipes = value; }
        }
    }
}